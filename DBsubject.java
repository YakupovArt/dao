import Dao.Subject;
import Dao.SubjectDao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBsubject implements SubjectDao{
    private final Connection connection;

    public DBsubject(Connection connection){
        this.connection = connection;
    }

    @Override
    public void create(Subject subject) throws SQLException {
        String sql = "INSERT INTO subject (subject_name,subject_theme)" +
                "VALUES (?, ?)";
        PreparedStatement stm = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);

        stm.setString(1, subject.getSubjectName());
        stm.setString(2, subject.getSubjectTheme());
        stm.executeUpdate();

        ResultSet generatedKeys = stm.getGeneratedKeys();

        if(generatedKeys.next()){
            subject.setId(generatedKeys.getInt(1));
        }

    }

    @Override
    public Subject read(int key) throws SQLException {
        String sql = "SELECT * FROM subject WHERE subject_id = ?;";
        PreparedStatement stm = connection.prepareStatement(sql);

        stm.setInt(1, key);

        ResultSet rs = stm.executeQuery();
        rs.next();

        return new Subject.Builder()
                .id(rs.getInt("subject_id"))
                .subjectName(rs.getString("subject_name"))
                .subjectTheme(rs.getString("subject_theme"))
                .build();

    }

    @Override
    public void update(Subject subject) throws SQLException {
        String sql =  "UPDATE subject SET subject_name= ?, subject_theme = ? WHERE subject_id= ?;";
        PreparedStatement stm = connection.prepareStatement(sql);

        stm.setString(1, subject.getSubjectName());
        stm.setString(2, subject.getSubjectTheme());
        stm.setInt(3, subject.getId());
        stm.executeUpdate();

    }

    @Override
    public void delete(int id) throws SQLException {
        String sql = "DELETE FROM subject WHERE subject_id = ?;";
        PreparedStatement stm = connection.prepareStatement(sql);

        stm.setInt(1, id);
        stm.executeUpdate();
    }

    @Override
    public List<Subject> getAll() throws SQLException {
        String sql = "SELECT * FROM subject;";
        PreparedStatement stm = connection.prepareStatement(sql);
        ResultSet rs = stm.executeQuery();
        List<Subject> list = new ArrayList<Subject>();
        while (rs.next()) {
            Subject subject = new Subject.Builder()
                    .id(rs.getInt("subject_id"))
                    .subjectName(rs.getString("subject_name"))
                    .subjectTheme(rs.getString("subject_theme"))
                    .build();
            list.add(subject);
        }
        return list;

    }
}
