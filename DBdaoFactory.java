import Dao.*;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBdaoFactory implements DaoFactory {
    private String user = "postgres";
    private String password = "templerss1";
    private String url = "jdbc:postgresql://localhost:5432/feedback";
    private Driver driver = new org.postgresql.Driver();

    public Connection getConnection() throws SQLException {
        return DriverManager.getConnection(url, user, password);
    }

    @Override
    public SubjectDao getSubjectDao(Connection connection) {
        return new DBsubject(connection);
    }

    @Override
    public StudentDao getStudentDao(Connection connection) {
        return new DBstudent(connection);
    }

    @Override
    public FeedbackDao getFeedbackDao(Connection connection) {
        return new DBfeedback(connection);
    }

    public DBdaoFactory() {
        try {
            DriverManager.registerDriver(driver);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
