import Dao.Student;
import Dao.StudentDao;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBstudent implements StudentDao {
    private final Connection connection;

    public DBstudent(Connection connection){
        this.connection = connection;
    }

    @Override
    public void create(Student student) throws SQLException {
        String sql = "INSERT INTO student (fio,gruppa)" +
        "VALUES (?, ?)";
        PreparedStatement stm = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);



        stm.setString(1, student.getFio());
        stm.setString(2, student.getGroup());
        stm.executeUpdate();

        ResultSet generatedKeys = stm.getGeneratedKeys();

        if(generatedKeys.next()){
            student.setId(generatedKeys.getInt(1));
        }

    }

    @Override
    public Student read(int key) throws SQLException {
        String sql = "SELECT * FROM student WHERE student_id = ?;";
        PreparedStatement stm = connection.prepareStatement(sql);

        stm.setInt(1, key);

        ResultSet rs = stm.executeQuery();
        rs.next();

        return new Student.Builder()
                .id(rs.getInt("student_id"))
                .fio(rs.getString("fio"))
                .group(rs.getString("gruppa"))
                .build();

    }

    @Override
    public void update(Student student) throws SQLException {
        String sql =  "UPDATE student SET fio= ?, gruppa = ? WHERE id= ?;";
        PreparedStatement stm = connection.prepareStatement(sql);

        stm.setString(1, student.getFio());
        stm.setString(2, student.getGroup());
        stm.setInt(3, student.getId());
        stm.executeUpdate();

    }

    @Override
    public void delete(int id) throws SQLException {
        String sql = "DELETE FROM student WHERE student_id = ?;";
        PreparedStatement stm = connection.prepareStatement(sql);

        stm.setInt(1, id);
        stm.executeUpdate();
    }

    @Override
    public List<Student> getAll() throws SQLException {
        String sql = "SELECT * FROM student;";
        PreparedStatement stm = connection.prepareStatement(sql);
        ResultSet rs = stm.executeQuery();
        List<Student> list = new ArrayList<Student>();
        while (rs.next()) {
            Student student = new Student.Builder()
                    .id(rs.getInt("student_id"))
                    .fio(rs.getString("fio"))
                    .group(rs.getString("gruppa"))
                    .build();
            list.add(student);
        }
        return list;

    }
}
