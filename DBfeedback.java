import Dao.Feedback;
import Dao.FeedbackDao;

import javax.xml.transform.Result;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class DBfeedback implements FeedbackDao{
    private final Connection connection;

    public DBfeedback(Connection connection){
        this.connection = connection;
    }

    @Override
    public void create(Feedback feedback) throws SQLException {
        String sql = "INSERT INTO feedback (feedback, date)" +
                "VALUES (?, ?)";
        PreparedStatement stm = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);


        stm.setString(1, feedback.getFeedback());
        stm.setString(2, feedback.getDate());
        stm.executeUpdate();

        ResultSet generatedKeys = stm.getGeneratedKeys();

        if(generatedKeys.next()){
            feedback.setId(generatedKeys.getInt(1));
        }
    }

    @Override
    public Feedback read(int key) throws SQLException {
        String sql = "SELECT * FROM feedback WHERE feedback_id = ?;";
        PreparedStatement stm = connection.prepareStatement(sql);

        stm.setInt(1, key);

        ResultSet rs = stm.executeQuery();
        rs.next();


        return new Feedback.Builder()
                .id(rs.getInt("feedback_id"))
                .studentId(rs.getInt("student_id"))
                .subjectId(rs.getInt("subject_id"))
                .feedback(rs.getString("feedback"))
                .date(rs.getString("date"))
                .build();

    }

    @Override
    public void update(Feedback feedback) throws SQLException {
        String sql =  "UPDATE feedback SET student_id = ?, subject_id = ?, feedback = ?, date = ? WHERE feedback_id= ?;";
        PreparedStatement stm = connection.prepareStatement(sql);

        stm.setInt(1, feedback.getStudentId());
        stm.setInt(2, feedback.getSubjectId());
        stm.setString(3, feedback.getFeedback());
        stm.setString(4, feedback.getDate());
        stm.setInt(5, feedback.getId());
        stm.executeUpdate();

    }

    @Override
    public void delete(int id) throws SQLException {
        String sql = "DELETE FROM feedback WHERE feedback_id = ?;";
        PreparedStatement stm = connection.prepareStatement(sql);

        stm.setInt(1, id);
        stm.executeUpdate();
    }

    @Override
    public List<Feedback> getAll() throws SQLException {
        String sql = "SELECT * FROM feedback;";
        PreparedStatement stm = connection.prepareStatement(sql);
        ResultSet rs = stm.executeQuery();
        List<Feedback> list = new ArrayList<Feedback>();
        while (rs.next()) {
            Feedback feedback = new Feedback.Builder()
                    .id(rs.getInt("feedback_id"))
                    .studentId(rs.getInt("student_id"))
                    .subjectId(rs.getInt("subject_id"))
                    .feedback(rs.getString("feedback"))
                    .date(rs.getString("date"))
                    .build();
            list.add(feedback);
        }
        return list;

    }
}
