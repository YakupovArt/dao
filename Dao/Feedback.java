package Dao;

public class Feedback {
    private int id;
    private int studentId;
    private int subjectId;
    private String feedback;
    private String date;

    public Feedback(Builder builder){
        this.id = builder.id;
        this.studentId = builder.studentId;
        this.subjectId = builder.subjectId;
        this.feedback = builder.feedback;
        this.date = builder.date;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public static class Builder {

        private int id;
        private int studentId;
        private int subjectId;
        private String feedback;
        private String date;


        public Builder  id(int id) {
            this.id = id;
            return this;
        }

        public Builder studentId(int subjectId) {
            this.studentId = subjectId;
            return this;
        }

        public Builder subjectId(int subjectId) {
            this.subjectId = subjectId;
            return this;
        }

        public Builder feedback(String feedback) {
            this.feedback = feedback;
            return this;
        }

        public Builder date(String date) {
            this.date = date;
            return this;
        }

        public Feedback build() {
            return new Feedback(this);
        }
    }

}
