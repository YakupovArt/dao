package Dao;

public class Student {
    private int id;
    private String fio;
    private String group;

    public Student(Builder builder) {
        this.id = builder.id;
        this.fio = builder.fio;
        this.group = builder.group;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public static class Builder {

        private int id;
        private String fio;
        private String group;


        public Builder id(int id) {
            this.id = id;
            return this;
        }

        public Builder fio(String fio) {
            this.fio = fio;
            return this;
        }

        public Builder group(String group) {
            this.group = group;
            return this;
        }

        public Student build() {
            return new Student(this);
        }
    }
}
