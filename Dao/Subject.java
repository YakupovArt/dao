package Dao;

public class Subject {
    private int id;
    private String subjectName;
    private String subjectTheme;

    public Subject(Builder builder){
        this.id = builder.id;
        this.subjectName = builder.subjectName;
        this.subjectTheme = builder.subjectTheme;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public String getSubjectTheme() {
        return subjectTheme;
    }

    public void setSubjectTheme(String subjectTheme) {
        this.subjectTheme = subjectTheme;
    }

    public static class Builder {

        private int id;
        private String subjectName;
        private String subjectTheme;

        public Builder id(int id) {
            this.id = id;
            return this;
        }

        public Builder subjectName(String subjectName) {
            this.subjectName = subjectName;
            return this;
        }

        public Builder subjectTheme(String subjectTheme) {
            this.subjectTheme = subjectTheme;
            return this;
        }

        public Subject build() {
            return new Subject(this);
        }
    }
}
