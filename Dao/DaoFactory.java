package Dao;

import java.sql.Connection;
import java.sql.SQLException;

public interface DaoFactory {
    public Connection getConnection() throws SQLException;
    public SubjectDao getSubjectDao(Connection connection);
    public StudentDao getStudentDao(Connection connection);
    public FeedbackDao getFeedbackDao(Connection connection);
}
