package Dao;

import java.sql.SQLException;
import java.util.List;

public interface SubjectDao {
    public void create(Subject subject) throws SQLException;
    public Subject read(int key) throws SQLException;
    public void update(Subject student) throws SQLException;
    public void delete(int id) throws SQLException;
    public List<Subject> getAll() throws SQLException;
}
