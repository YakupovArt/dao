package Dao;

import java.sql.SQLException;
import java.util.List;

public interface StudentDao {
    public void create(Student student) throws SQLException;
    public Student read(int key) throws SQLException;
    public void update(Student student) throws SQLException;
    public void delete(int id) throws SQLException;
    public List<Student> getAll() throws SQLException;
}
