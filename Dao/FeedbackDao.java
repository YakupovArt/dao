package Dao;

import java.sql.SQLException;
import java.util.List;

public interface FeedbackDao {
    public void create(Feedback feedback) throws SQLException;
    public Feedback read(int key) throws SQLException;
    public void update(Feedback student) throws SQLException;
    public void delete(int id) throws SQLException;
    public List<Feedback> getAll() throws SQLException;
}
